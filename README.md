# Mistakes I always Make...

## 1 - Trying a disconnected switch...

Try fix by changing `function()`, `command`, or `setting`, which has 0 effect on the outcome of the problem.
Like keep flipping a disconnected switch.

## 2 - Being out of Character...

- A `'` is not a `"`, and a `<tab>` is not `    `.
- `ASCII` is not `UTF-8` and `UTF-8` is not `ISO-8859-1` and `UTF-16`.

Many times, small characters can break things, almost invisible, and encodings can become explosions...

## 3 - Access Denied...

Authorisation problems are sometimes limiting you perform actions,
without realising its user or authorisation related. 

## 4 - Incompatible Components...

Using components with incompatible version, build, type...
Everything seems good, except that little version number...

## Solving Patterns

* **Check the switch** : Check if the lightswitch works at all, with extreme cases. If you think that function has influence, use extreme input.
* **Walk from working example** : Take a working example, and walk in small steps to your desired end result.
* **Verify what is not the problem** : Verify the things that absolutely have nothing to do with the problem and work for sure. Take time to verify indeed what is absolutely not the problem.